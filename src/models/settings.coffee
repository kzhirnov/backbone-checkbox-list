class CheckboxList.Settings extends Backbone.Model
	defaults:
		LiConstructor : CheckboxList.Li
		EmptyLiConstructor : CheckboxList.EmptyLi
		UlConstructor : CheckboxList.Ul
		SearchConstructor : CheckboxList.Search
		checkboxName : 'item'
		searchParamName : 'title'
		searchTimeout : 10
		highlightedText : ''
		emptyText : 'No data'
		itemInputType : 'checkbox'

	makeLi : (options = {}) ->
		return @makeViewInstance @get('LiConstructor'), options

	makeEmptyLi : (options = {}) ->
		return @makeViewInstance @get('EmptyLiConstructor'), options

	makeUl : (options = {}) ->
		return @makeViewInstance @get('UlConstructor'), options

	makeSearch : (options) ->
		return @makeViewInstance @get('SearchConstructor'), options

	makeViewInstance : (Constructor, options) ->
		_.extend options, {
			settings : @
		}

		return new Constructor(options)
