class CheckboxList.EmptyLi extends CheckboxList.Li
	className : 'empty'

	getTpl : ->
		return @settings.get('emptyText')