class CheckboxList.View extends Backbone.View
	constructor : (options) ->
		_.extend @, _.pick(options, ['settings'])

		if !(@settings instanceof CheckboxList.Settings)
			@settings = new CheckboxList.Settings @settings

		super

	remove : ->
		@settings = null
		@model = null
		@collection = null

		return super