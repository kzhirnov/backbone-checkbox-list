class CheckboxList.Ul extends CheckboxList.View
	tagName : 'ul'

	className : 'list-unstyled small'

	initialize : (options) ->
		@items = []

		@listenTo @collection, 'sync', @render

		if @settings.get('itemInputType') == 'radio'
			@listenTo @collection, 'onBeforeCheckedChanged', (changedModel) =>
				_.each @collection.where({checked:true}), (model) =>
					model.set 'checked', false

	render : ->
		@clearItems()

		if @collection.length > 0
			@collection.each (model) =>
				item = @settings.makeLi {model:model}
				@$el.append item.render().$el
				@items.push item
		else
			item = @settings.makeEmptyLi()
			@$el.append item.render().$el
			@items.push item

		return @

	remove : ->
		@clearItems()

		return super

	clearItems : ->
		_.each @items, (view, key) ->
			view.remove()

		@items = []
