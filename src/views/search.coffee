class CheckboxList.Search extends CheckboxList.View
	className : 'form-group has-feedback'

	initialize : ->
		@searchString = ''
		@timeoutId = null

	events : ->
		return {
			"keyup input[name='search']" : "onKeyUp"
		}

	render : ->
		@$el.html @getTpl()

		return @

	getTpl : ->
		return """
<input type="text" class="form-control input-sm" name="search" value="">
<span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
"""

	onKeyUp : ->
		val = @$('input[name="search"]').val()

		if val == @searchString
			return

		@searchString = val
		data = {}
		data[@settings.get('searchParamName')] = val

		if @timeoutId
			clearTimeout @timeoutId

		@timeoutId = setTimeout =>
			@collection.fetch {
				data : data
				reset : true
			}

			@settings.set 'highlightedText', val
		, @settings.get('searchTimeout')