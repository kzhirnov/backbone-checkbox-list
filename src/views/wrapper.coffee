class CheckboxList.Wrapper extends CheckboxList.View
	className : 'checkbox-list'

	initialize : ->
		@ul = null
		@search = null

		if _.isArray(@collection)
			@collection = new Backbone.Collection @collection

	render : ->
		if @settings.get('searchParamName') isnt false
			@search = @settings.makeSearch {collection:@collection}
			@$el.append @search.render().$el

		@ul = @settings.makeUl {collection:@collection}
		@$el.append @ul.render().$el

		return @

	remove : ->
		@ul.remove()
		@ul = null

		if @search
			@search.remove()
			@search = null

		return super