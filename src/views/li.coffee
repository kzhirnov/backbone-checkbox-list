class CheckboxList.Li extends CheckboxList.View
	tagName : 'li'

	initialize : ->
		super

		if @model
			@listenTo @model, 'change:checked', @onModelCheckedChanged

		return

	events : ->
		return {
			"change input" : "onInputChanged"
		}

	render : ->
		@$el.html @getTpl()

		return @

	getTitle : ->
		title = @model.get 'title'
		return @prepareTitle title

	prepareTitle : (title) ->
		highlightedText = @settings.get 'highlightedText'

		if highlightedText != ''
			regExp = new RegExp highlightedText, 'i'
			title = title.replace regExp, "<b>#{highlightedText}</b>"

		return title

	getChecked : ->
		return @model.get 'checked'

	getTpl : ->
		checked = if @getChecked() then 'checked="checked"' else ''

		switch @settings.get('itemInputType')
			when "checkbox"
				classAttr = "checkbox"
				input = """
					<input type="checkbox" name="#{@settings.get('checkboxName')}" value="#{@model.id}" #{checked} />
				"""

			when "radio"
				classAttr = "radio"
				input = """
					<input type="radio" name="#{@settings.get('checkboxName')}" value="#{@model.id}" #{checked} />
				"""

			else
				classAttr = ""
				input = ""

		return """
			<div class="#{classAttr}">
				<label>
					#{input} #{@getTitle()}
				</label>
			</div>
		"""

	onModelCheckedChanged : ->
		@$("input").prop 'checked', @model.get('checked')

	onInputChanged : ->
		@model.trigger 'onBeforeCheckedChanged', @model

		@model.set 'checked', @$("input").prop('checked')