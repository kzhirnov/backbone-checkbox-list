(function (root, factory) {

  if (typeof define === "function" && define.amd) {
    // AMD (+ global for extensions)
    define(["underscore", "backbone"], function (_, Backbone) {
      return (root.CheckboxList = factory(_, Backbone));
    });
  } else if (typeof exports === "object") {
    // CommonJS
    module.exports = factory(require("underscore"), require("backbone"));
  } else {
    // Browser
    root.CheckboxList = factory(root._, root.Backbone);
  }}(this, function (_, Backbone) {

  "use strict";

var CheckboxList,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

CheckboxList = {};

CheckboxList.View = (function(superClass) {
  extend(View, superClass);

  function View(options) {
    _.extend(this, _.pick(options, ['settings']));
    if (!(this.settings instanceof CheckboxList.Settings)) {
      this.settings = new CheckboxList.Settings(this.settings);
    }
    View.__super__.constructor.apply(this, arguments);
  }

  View.prototype.remove = function() {
    this.settings = null;
    this.model = null;
    this.collection = null;
    return View.__super__.remove.apply(this, arguments);
  };

  return View;

})(Backbone.View);

CheckboxList.Li = (function(superClass) {
  extend(Li, superClass);

  function Li() {
    return Li.__super__.constructor.apply(this, arguments);
  }

  Li.prototype.tagName = 'li';

  Li.prototype.initialize = function() {
    Li.__super__.initialize.apply(this, arguments);
    if (this.model) {
      this.listenTo(this.model, 'change:checked', this.onModelCheckedChanged);
    }
  };

  Li.prototype.events = function() {
    return {
      "change input": "onInputChanged"
    };
  };

  Li.prototype.render = function() {
    this.$el.html(this.getTpl());
    return this;
  };

  Li.prototype.getTitle = function() {
    var title;
    title = this.model.get('title');
    return this.prepareTitle(title);
  };

  Li.prototype.prepareTitle = function(title) {
    var highlightedText, regExp;
    highlightedText = this.settings.get('highlightedText');
    if (highlightedText !== '') {
      regExp = new RegExp(highlightedText, 'i');
      title = title.replace(regExp, "<b>" + highlightedText + "</b>");
    }
    return title;
  };

  Li.prototype.getChecked = function() {
    return this.model.get('checked');
  };

  Li.prototype.getTpl = function() {
    var checked, classAttr, input;
    checked = this.getChecked() ? 'checked="checked"' : '';
    switch (this.settings.get('itemInputType')) {
      case "checkbox":
        classAttr = "checkbox";
        input = "<input type=\"checkbox\" name=\"" + (this.settings.get('checkboxName')) + "\" value=\"" + this.model.id + "\" " + checked + " />";
        break;
      case "radio":
        classAttr = "radio";
        input = "<input type=\"radio\" name=\"" + (this.settings.get('checkboxName')) + "\" value=\"" + this.model.id + "\" " + checked + " />";
        break;
      default:
        classAttr = "";
        input = "";
    }
    return "<div class=\"" + classAttr + "\">\n	<label>\n		" + input + " " + (this.getTitle()) + "\n	</label>\n</div>";
  };

  Li.prototype.onModelCheckedChanged = function() {
    return this.$("input").prop('checked', this.model.get('checked'));
  };

  Li.prototype.onInputChanged = function() {
    this.model.trigger('onBeforeCheckedChanged', this.model);
    return this.model.set('checked', this.$("input").prop('checked'));
  };

  return Li;

})(CheckboxList.View);

CheckboxList.EmptyLi = (function(superClass) {
  extend(EmptyLi, superClass);

  function EmptyLi() {
    return EmptyLi.__super__.constructor.apply(this, arguments);
  }

  EmptyLi.prototype.className = 'empty';

  EmptyLi.prototype.getTpl = function() {
    return this.settings.get('emptyText');
  };

  return EmptyLi;

})(CheckboxList.Li);

CheckboxList.Ul = (function(superClass) {
  extend(Ul, superClass);

  function Ul() {
    return Ul.__super__.constructor.apply(this, arguments);
  }

  Ul.prototype.tagName = 'ul';

  Ul.prototype.className = 'list-unstyled small';

  Ul.prototype.initialize = function(options) {
    this.items = [];
    this.listenTo(this.collection, 'sync', this.render);
    if (this.settings.get('itemInputType') === 'radio') {
      return this.listenTo(this.collection, 'onBeforeCheckedChanged', (function(_this) {
        return function(changedModel) {
          return _.each(_this.collection.where({
            checked: true
          }), function(model) {
            return model.set('checked', false);
          });
        };
      })(this));
    }
  };

  Ul.prototype.render = function() {
    var item;
    this.clearItems();
    if (this.collection.length > 0) {
      this.collection.each((function(_this) {
        return function(model) {
          var item;
          item = _this.settings.makeLi({
            model: model
          });
          _this.$el.append(item.render().$el);
          return _this.items.push(item);
        };
      })(this));
    } else {
      item = this.settings.makeEmptyLi();
      this.$el.append(item.render().$el);
      this.items.push(item);
    }
    return this;
  };

  Ul.prototype.remove = function() {
    this.clearItems();
    return Ul.__super__.remove.apply(this, arguments);
  };

  Ul.prototype.clearItems = function() {
    _.each(this.items, function(view, key) {
      return view.remove();
    });
    return this.items = [];
  };

  return Ul;

})(CheckboxList.View);

CheckboxList.Search = (function(superClass) {
  extend(Search, superClass);

  function Search() {
    return Search.__super__.constructor.apply(this, arguments);
  }

  Search.prototype.className = 'form-group has-feedback';

  Search.prototype.initialize = function() {
    this.searchString = '';
    return this.timeoutId = null;
  };

  Search.prototype.events = function() {
    return {
      "keyup input[name='search']": "onKeyUp"
    };
  };

  Search.prototype.render = function() {
    this.$el.html(this.getTpl());
    return this;
  };

  Search.prototype.getTpl = function() {
    return "<input type=\"text\" class=\"form-control input-sm\" name=\"search\" value=\"\">\n<span class=\"glyphicon glyphicon-search form-control-feedback\" aria-hidden=\"true\"></span>";
  };

  Search.prototype.onKeyUp = function() {
    var data, val;
    val = this.$('input[name="search"]').val();
    if (val === this.searchString) {
      return;
    }
    this.searchString = val;
    data = {};
    data[this.settings.get('searchParamName')] = val;
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
    return this.timeoutId = setTimeout((function(_this) {
      return function() {
        _this.collection.fetch({
          data: data,
          reset: true
        });
        return _this.settings.set('highlightedText', val);
      };
    })(this), this.settings.get('searchTimeout'));
  };

  return Search;

})(CheckboxList.View);

CheckboxList.Wrapper = (function(superClass) {
  extend(Wrapper, superClass);

  function Wrapper() {
    return Wrapper.__super__.constructor.apply(this, arguments);
  }

  Wrapper.prototype.className = 'checkbox-list';

  Wrapper.prototype.initialize = function() {
    this.ul = null;
    this.search = null;
    if (_.isArray(this.collection)) {
      return this.collection = new Backbone.Collection(this.collection);
    }
  };

  Wrapper.prototype.render = function() {
    if (this.settings.get('searchParamName') !== false) {
      this.search = this.settings.makeSearch({
        collection: this.collection
      });
      this.$el.append(this.search.render().$el);
    }
    this.ul = this.settings.makeUl({
      collection: this.collection
    });
    this.$el.append(this.ul.render().$el);
    return this;
  };

  Wrapper.prototype.remove = function() {
    this.ul.remove();
    this.ul = null;
    if (this.search) {
      this.search.remove();
      this.search = null;
    }
    return Wrapper.__super__.remove.apply(this, arguments);
  };

  return Wrapper;

})(CheckboxList.View);

CheckboxList.Settings = (function(superClass) {
  extend(Settings, superClass);

  function Settings() {
    return Settings.__super__.constructor.apply(this, arguments);
  }

  Settings.prototype.defaults = {
    LiConstructor: CheckboxList.Li,
    EmptyLiConstructor: CheckboxList.EmptyLi,
    UlConstructor: CheckboxList.Ul,
    SearchConstructor: CheckboxList.Search,
    checkboxName: 'item',
    searchParamName: 'title',
    searchTimeout: 10,
    highlightedText: '',
    emptyText: 'No data',
    itemInputType: 'checkbox'
  };

  Settings.prototype.makeLi = function(options) {
    if (options == null) {
      options = {};
    }
    return this.makeViewInstance(this.get('LiConstructor'), options);
  };

  Settings.prototype.makeEmptyLi = function(options) {
    if (options == null) {
      options = {};
    }
    return this.makeViewInstance(this.get('EmptyLiConstructor'), options);
  };

  Settings.prototype.makeUl = function(options) {
    if (options == null) {
      options = {};
    }
    return this.makeViewInstance(this.get('UlConstructor'), options);
  };

  Settings.prototype.makeSearch = function(options) {
    return this.makeViewInstance(this.get('SearchConstructor'), options);
  };

  Settings.prototype.makeViewInstance = function(Constructor, options) {
    _.extend(options, {
      settings: this
    });
    return new Constructor(options);
  };

  return Settings;

})(Backbone.Model);
  return CheckboxList;
}));